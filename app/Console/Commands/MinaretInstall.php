<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MinaretInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minaret:install {dev}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this is test command to install minaret platform package';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        switch ($this->argument('dev')) {
            case 'dev':
                Artisan::call('voyager:install');
                Artisan::call('voyager:admin minaret@admin.com --create');
                return 0;
                break;
        }
    }
}
